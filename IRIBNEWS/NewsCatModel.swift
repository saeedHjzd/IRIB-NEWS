//
//  NewsCatModel.swift
//  IRIBNEWS
//
//  Created by Hamidreza Bistooni on 5/26/18.
//  Copyright © 2018 Hamidreza Bistooni. All rights reserved.
//

import Foundation

class NewsCatModel {
    
    static let newsCategories = [NewsCatModel()]
    
    private var _record_id = ""
    private var _title = ""
    
    var record_id: String {
        get {
            return _record_id
        }
        set {
            _record_id = newValue
        }
    }
    
    var title: String {
        get {
            return _title
        }
        set {
            _title = newValue
        }
    }
    
}
