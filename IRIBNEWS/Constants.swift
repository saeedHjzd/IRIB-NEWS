//
//  Constants.swift
//  IRIBNEWS
//
//  Created by Hamidreza Bistooni on 5/26/18.
//  Copyright © 2018 Hamidreza Bistooni. All rights reserved.
//

import Foundation

//  MARK: Keys
let NEWS_CAT_KEY = "newsCategoryKey"


//  MARK: Urls
let NEWS_CATEGORIES = "http://www.iribnews.ir/fa/webservice/json/services"
let NEWS_URL = "http://www.iribnews.ir/fa/webservice/json/news/service/"
