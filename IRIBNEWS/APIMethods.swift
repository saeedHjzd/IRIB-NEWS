//
//  APIMethods.swift
//  IRIBNEWS
//
//  Created by Hamidreza Bistooni on 5/26/18.
//  Copyright © 2018 Hamidreza Bistooni. All rights reserved.
//

import Foundation
import Alamofire

class APIMethods {
    
    static let sharedInstance = APIMethods()
    
    
    //  MARK: Post Methods
    func postMethod(parametrs: Dictionary<String,Any>, andUrl url:String,success succeeded: @escaping ((Any) -> ()),failure failed: @escaping ((Any) -> ()) ) {
        
        Alamofire.request(url, method: .post, parameters: parametrs).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    failed(error.localizedDescription)
                }
                return
            }
            
            guard let value = response.result.value else {return}
            succeeded(value)
            
        }
    }
    
    
    func postMethodWithHeaders(parametrs: Dictionary<String,Any>, andUrl url:String, andHeaders header:[String : String] , success succeeded: @escaping ((Any) -> ()),failure failed: @escaping ((Any) -> ()) ) {
        
        Alamofire.request(url, method: .post, parameters: parametrs, encoding: JSONEncoding.default, headers: header).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    failed(error.localizedDescription)
                }
                return
            }
            
            guard let value = response.result.value else {return}
            succeeded(value)
            
        }
    }
    
    
    func postMethodWithImage(imageData:Data, parametrs: Dictionary<String,Any>, headers: [String:String] , andUrl url:String,success succeeded: @escaping ((Any) -> ()),failure failed: @escaping ((Any) -> ()) ) {
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "photo",fileName: "image.jpg", mimeType: "image/jpg")
            for (key, value) in parametrs {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:url,
                         method:.post,
                         headers: headers)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response.result.value!)
                    
                    succeeded(response.result.value!)
                }
                
                
                
            case .failure(let encodingError):
                print(encodingError)
                
                failed(encodingError)
            }
        }
        
    }
    
    
    
    //    MARK: Get Methods
    func getMethod(parametrs: Dictionary<String,Any>, andUrl url:String,success succeeded: @escaping ((Any) -> ()),failure failed: @escaping (Any) -> () ) {
        
        Alamofire.request(url, method: .get, parameters: parametrs).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    failed(error.localizedDescription)
                }
                return
            }
            
            guard let value = response.result.value else {return}
            succeeded(value)
            
        }
    }
    
    
    func getMethodWithHeaders(parametrs: Dictionary<String,Any>, andHeaders header:[String : String], andUrl url:String,success succeeded: @escaping ((Any) -> ()),failure failed: @escaping (Any) -> () ) {
        
        
        Alamofire.request(url, method: .get, parameters: parametrs, encoding: URLEncoding.default, headers: header).validate().responseJSON { (response) in
            guard response.result.isSuccess else {
                if let error = response.result.error {
                    failed(error.localizedDescription)
                }
                return
            }
            
            guard let value = response.result.value else {return}
            succeeded(value)
        }
        
        
    }
  
}

