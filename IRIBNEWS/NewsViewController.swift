//
//  NewsViewController.swift
//  IRIBNEWS
//
//  Created by Hamidreza Bistooni on 5/26/18.
//  Copyright © 2018 Hamidreza Bistooni. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APIServices.newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as? NewsTableViewCell {
            let news = APIServices.newsArray[indexPath.row]
            
            cell.newsTimeLabel.text = "۵ دقیقه"
            cell.newsCatLabel.text = "سیاست داخلی"
            cell.newsTitleLabel.text = news.title
            cell.newsImage.image = #imageLiteral(resourceName: "icon_1024_2")
            
            return cell
        }else {
        
            return NewsTableViewCell()
            
        }
    }
    
    
    @IBOutlet weak var newsTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        APIServices.sharedInstance.getNews(service_id: "1", page: "1", success: { (success) in
            
            if success {
                self.newsTableView.reloadData()
                print(APIServices.newsArray.count)
            }
            
        }) { (error) in
            debugPrint(error)
        }

        newsTableView.delegate = self
        newsTableView.dataSource = self
        
    }

}
