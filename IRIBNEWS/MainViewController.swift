//
//  ViewController.swift
//  IRIBNEWS
//
//  Created by Hamidreza Bistooni on 5/26/18.
//  Copyright © 2018 Hamidreza Bistooni. All rights reserved.
//

import UIKit
import CarbonKit

class MainViewController: UIViewController, CarbonTabSwipeNavigationDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        switch index {
        case 0:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NewsTable")
            return vc
            
        case 1:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NewsTable")
            return vc
            
        case 2:
            let vc = storyBoard.instantiateViewController(withIdentifier: "NewsTable")
            return vc
            
        default:
            return storyBoard.instantiateViewController(withIdentifier: "NewsTable")
        }

        
    }
    
    
    @IBOutlet weak var carbonContainerView: UIView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        APIServices.sharedInstance.getMainNewsCatTitlesAndRecordID(success: { (success) in
            
            if success {
                print(APIServices.newsCategoryArray[1].title)
                
                var items = [String]()
                
                for newsCat in APIServices.newsCategoryArray {
                    let title = newsCat.title
                    items.append(title)
                }
                
                let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
                carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.carbonContainerView)
                
                carbonTabSwipeNavigation.setIndicatorColor(#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                        carbonTabSwipeNavigation.setNormalColor(#colorLiteral(red: 0.0727007772, green: 0.0727007772, blue: 0.0727007772, alpha: 1), font: UIFont.systemFont(ofSize: 12))
                        carbonTabSwipeNavigation.setSelectedColor(#colorLiteral(red: 0.0727007772, green: 0.0727007772, blue: 0.0727007772, alpha: 1), font: UIFont.systemFont(ofSize: 12))
                
                carbonTabSwipeNavigation.toolbar.barTintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                carbonTabSwipeNavigation.toolbar.isTranslucent = false
                
                carbonTabSwipeNavigation.setCurrentTabIndex(1, withAnimation: false)
                
            }
            
        }) { (error) in
            debugPrint(error)
        }
        

        
        
    }
    
}

