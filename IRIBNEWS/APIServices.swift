//
//  APIServices.swift
//  IRIBNEWS
//
//  Created by Hamidreza Bistooni on 5/26/18.
//  Copyright © 2018 Hamidreza Bistooni. All rights reserved.
//

import Foundation
import UIKit

class APIServices {
    
    static let sharedInstance = APIServices()
    
    static var newsCategoryArray = [NewsCatModel()]
    static var newsArray = [NewsModel()]
    
    
    func getMainNewsCatTitlesAndRecordID (success succeeded:@escaping(Bool)->(), failure failed:@escaping(Any)->()) {
        
        APIMethods.sharedInstance.getMethod(parametrs: [:], andUrl: NEWS_CATEGORIES, success: { (data) in
            
            guard let newsCatArray = data as? [[String:Any]] else {return}
            
            for newsCat in newsCatArray {
                let newsCategory = NewsCatModel()
                
                if let newsTitle = newsCat["title"] as? String {
                    newsCategory.title = newsTitle
                }
                
                if let newsID = newsCat["record_id"] as? String {
                    newsCategory.record_id = newsID
                }
                
                APIServices.newsCategoryArray.append(newsCategory)
                
                print(newsCategory.title)
            }
            succeeded(true)
            
        }) { (error) in
            failed(error)
        }
    }
    
    
    func getNews (service_id:String, page:String , success succeeded:@escaping(Bool)->(), failure failed:@escaping(Any)->()) {
        
        APIMethods.sharedInstance.getMethod(parametrs: [:], andUrl: NEWS_URL+"\(service_id)/"+"\(page)", success: { (data) in
            
            guard let newsArray = data as? [[String:Any]] else {return}
            
            for news in newsArray {
                let newsobj = NewsModel()
                
                if let newsTitle = news["title"] as? String {
                    newsobj.title = newsTitle
                }
                
                if let newsID = news["record_id"] as? String {
                    newsobj.record_id = newsID
                }
                
                APIServices.newsArray.append(newsobj)
                
                print(newsobj.title)
            }
            succeeded(true)
            
        }) { (error) in
            failed(error)
        }
        
    }
    

}
